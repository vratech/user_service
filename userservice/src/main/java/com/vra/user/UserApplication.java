/**
 * 
 */
package com.vra.user;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author naveen
 *
 * @date 31-Dec-2018
 */
@SpringBootApplication
@ComponentScan({ "com.vra.user", "com.vra.core" })
public class UserApplication extends SpringBootServletInitializer {

}