/**
 * 
 */
package com.vra.user.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.vra.core.sqljpa.entity.AbstractJpaEntity;
import com.vra.core.user.enums.UserType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author naveen
 *
 * @date 31-Dec-2018
 */
@Getter
@Setter
@ToString(callSuper = true, exclude = "password")
@Table(name = "login_details")
@Entity(name = "login_details")
public class LoginDetailsEntity extends AbstractJpaEntity {

	private static final long serialVersionUID = -1329048807732229315L;

	@Column(name = "user_id", nullable = false)
	private String userId;

	@Column(name = "username", nullable = false)
	private String username;

	@Column(name = "password", nullable = false)
	private String password;

	@Enumerated(EnumType.STRING)
	@Column(name = "user_type", nullable = false)
	private UserType userType;

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

}