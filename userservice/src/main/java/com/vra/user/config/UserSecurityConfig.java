/**
 * 
 */
package com.vra.user.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vra.core.security.SecurityModuleBuilder;
import com.vra.core.security.interceptor.AuthInterceptor;
import com.vra.core.security.service.AuthService;
import com.vra.core.security.service.impl.AuthServiceImpl;

/**
 * @author naveen
 *
 * @date 31-Dec-2018
 */
@Configuration
public class UserSecurityConfig implements WebMvcConfigurer {

	@Value("${enable.acl:true}")
	private boolean enableAcl;

	@Value("${enable.cors:false}")
	private boolean enableCors;

	@Value("${auth.service.url}")
	private String authServiceUrl;

	@Value("${user.service.url}")
	private String userServiceUrl;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public void addInterceptors(InterceptorRegistry interceptorRegistry) {
		interceptorRegistry
				.addInterceptor(authInterceptor())
				.addPathPatterns("/**")
				.excludePathPatterns(
						"/pingMe",
						"/api/**",
						"/login",
						"/v2/api-docs",
						"/configuration/ui",
						"/swagger-resources/**",
						"/configuration/security",
						"/swagger-ui.html",
						"/webjars/**");

	}

	@Bean
	public AuthInterceptor authInterceptor() {
		return new SecurityModuleBuilder()
				.corsSupport(true)
				.objectMapper(objectMapper)
				.corsSupport(enableCors)
				.authServiceUrl(authServiceUrl)
				.userServiceUrl(userServiceUrl)
				.enableUrlBasedAuthorization(enableAcl)
				.build();
	}

	@Bean
	public AuthService authService() {
		return new AuthServiceImpl(objectMapper);
	}
}